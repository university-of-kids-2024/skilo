plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.jetbrains.kotlin.android)
    `maven-publish`
}

android {
    namespace = "com.ocado.uok.skilo.scanner"
    compileSdk = 34
    buildToolsVersion = "35.0.0"

    group = "com.ocado.uok.skilo"
    version = "2.0.0-rc.1"

    defaultConfig {
        minSdk = 24
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    testOptions {
        targetSdk = 34
    }
    lint {
        targetSdk = 34
    }
    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android.txt"),
                "proguard-rules.pro"
            )
        }
        debug {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    buildFeatures {
        viewBinding = true
    }
    publishing {
        publishing {
            singleVariant("release") {
                withSourcesJar()
            }
        }
    }
}

publishing {
    publications {
        register<MavenPublication>("release") {
            version = version.toString()

            afterEvaluate {
                from(components["release"])
            }
        }
        register<MavenPublication>("debug") {
            version = version.toString().substringBefore("-") + "-SNAPSHOT"

            afterEvaluate {
                from(components["debug"])
            }
        }
    }
    repositories {
        mavenLocal()
    }
}

dependencies {
    implementation(fileTree("libs") { include("*.jar") })

    coreLibraryDesugaring(libs.desugar.jdk.libs) // For Java 8 features such as java.time, java.util.stream, etc.

    api(libs.androidx.appcompat)
    api(libs.androidx.camera.camera2)
    api(libs.androidx.camera.mlkit.vision)
    api(libs.androidx.core.ktx)
    api(libs.androidx.lifecycle.viewmodel.android)
    api(libs.androidx.navigation.fragment.ktx)
    api(libs.barcode.scanning)
    api(libs.kotlin.reflect)
    api(libs.kotlinx.coroutines.android)

    testImplementation(libs.junit)

    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(libs.androidx.junit)
}
