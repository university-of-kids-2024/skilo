package com.ocado.uok.skilo.scanner

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.Drawable
import com.google.mlkit.vision.barcode.common.Barcode

/**
 * A Drawable that handles displaying a QR Code's data and a bounding box around the QR code.
 */
class BarcodeDrawable(private val barcode: Barcode) : Drawable() {

    private val boundingRectPaint = Paint().apply {
        style = Paint.Style.STROKE
        color = Color.YELLOW
        strokeWidth = 5F
        alpha = 200
    }

    private val contentRectPaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.YELLOW
        alpha = 255
    }

    private val contentTextPaint = Paint().apply {
        color = Color.DKGRAY
        alpha = 255
        textSize = 36F
    }

    private val contentPadding = 25
    private var textWidth = contentTextPaint.measureText(barcode.displayValue).toInt()

    override fun draw(canvas: Canvas) {
        barcode.boundingBox?.let {
            canvas.drawRect(it, boundingRectPaint)
            canvas.drawRect(
                Rect(
                    it.left,
                    it.bottom + contentPadding / 2,
                    it.left + textWidth + contentPadding * 2,
                    it.bottom + contentTextPaint.textSize.toInt() + contentPadding
                ),
                contentRectPaint
            )
            canvas.drawText(
                barcode.displayValue ?: "",
                (it.left + contentPadding).toFloat(),
                (it.bottom + contentPadding * 2).toFloat(),
                contentTextPaint
            )
        }
    }

    override fun setAlpha(alpha: Int) {
        boundingRectPaint.alpha = alpha
        contentRectPaint.alpha = alpha
        contentTextPaint.alpha = alpha
    }

    override fun setColorFilter(colorFiter: ColorFilter?) {
        boundingRectPaint.colorFilter = colorFilter
        contentRectPaint.colorFilter = colorFilter
        contentTextPaint.colorFilter = colorFilter
    }

    @Deprecated("Deprecated in Java")
    override fun getOpacity(): Int = PixelFormat.TRANSLUCENT
}
