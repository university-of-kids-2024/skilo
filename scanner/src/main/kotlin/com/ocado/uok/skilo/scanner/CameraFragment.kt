package com.ocado.uok.skilo.scanner

import android.Manifest.permission.CAMERA
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.camera.core.ImageAnalysis.COORDINATE_SYSTEM_VIEW_REFERENCED
import androidx.camera.mlkit.vision.MlKitAnalyzer
import androidx.camera.view.LifecycleCameraController
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.ocado.uok.skilo.scanner.databinding.FragmentCameraBinding
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class CameraFragment : Fragment() {

    private val scannerViewModel: ScannerViewModel by activityViewModels()

    private lateinit var binding: FragmentCameraBinding

    private lateinit var cameraExecutor: Executor
    private lateinit var barcodeScanner: BarcodeScanner

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCameraBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraExecutor = Executors.newFixedThreadPool(4)

        checkPermissions()

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                scannerViewModel.analyzedFlow
                    .windowed(10)
                    .map { it.filterNotNull().lastOrNull() }
                    .collect { barcode: Barcode? ->
                        if (barcode == null) {
                            Log.d(TAG, "No barcode found")
                        }
                        updateUi(barcode)
                    }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Closing barcodeScanner and clearImageAnalysisAnalyzer()")
        scannerViewModel.clearState()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun updateUi(barcode: Barcode?) {
        binding.cameraPreviewView.overlay.clear()
        if (barcode != null) {
            binding.cameraPreviewView.setOnTouchListener { _: View, motionEvent: MotionEvent ->
                motionEvent.run {
                    if (
                        action == ACTION_DOWN &&
                        barcode.boundingBox?.contains(x.toInt(), y.toInt()) == true
                    ) {
                        scannerViewModel.scan(barcode)
                        Log.d(TAG, "Clicked on QR code with text: ${barcode.displayValue}")
                    }
                }

                true
            }
            binding.cameraPreviewView.overlay.add(BarcodeDrawable(barcode))
        } else {
            binding.cameraPreviewView.setOnTouchListener { _, _ -> false }
        }
    }

    private fun checkPermissions() {
        registerForActivityResult(RequestPermission()) { result ->
            if (result) {
                startCamera()
            } else {
                showPermissionsToast()
            }
        }.launch(CAMERA)
    }

    private fun startCamera() {
        val cameraController = LifecycleCameraController(requireContext())

        barcodeScanner = BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
            .let { BarcodeScanning.getClient(it) }

        cameraController.setImageAnalysisAnalyzer(
            cameraExecutor,
            getMlKit(barcodeScanner, cameraExecutor)
        )

        cameraController.bindToLifecycle(this)
        binding.cameraPreviewView.controller = cameraController
        Log.d(TAG, "Camera started")
    }

    private fun showPermissionsToast() {
        Toast.makeText(requireContext(), "Permissions not granted by the user.", LENGTH_SHORT)
            .show()
    }

    private fun getMlKit(barcodeScanner: BarcodeScanner, executor: Executor) =
        MlKitAnalyzer(
            listOf(barcodeScanner),
            COORDINATE_SYSTEM_VIEW_REFERENCED,
            executor,
            onMlKitAnalyzerResult
        )

    private val onMlKitAnalyzerResult: (MlKitAnalyzer.Result) -> Unit = { result ->
        val barcodeResults: List<Barcode?>? = result.getValue(barcodeScanner)
        val qrCode = barcodeResults?.firstOrNull()
        scannerViewModel.analyze(qrCode)
    }

    companion object {
        private val TAG = CameraFragment::class.java.simpleName
    }

}
