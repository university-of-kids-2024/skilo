package com.ocado.uok.skilo.scanner

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.scan

fun <T> Flow<T>.windowed(size: Int): Flow<List<T>> =
    scan(emptyList<T>()) { acc, value ->
        (acc + value).takeLast(size)
    }
        .drop(size - 1)