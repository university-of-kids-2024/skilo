package com.ocado.uok.skilo.scanner

import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.barcode.common.Barcode.TYPE_TEXT
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow

class ScannerViewModel : ViewModel() {

    private val _analyzedFlow: MutableSharedFlow<Barcode?> = MutableSharedFlow(
        replay = 1,
        extraBufferCapacity = 10,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )
    val analyzedFlow: SharedFlow<Barcode?> = _analyzedFlow.asSharedFlow()

    private val _scannedFlow: MutableStateFlow<Barcode?> = MutableStateFlow(null)
    val scannedFlow: StateFlow<Barcode?> = _scannedFlow.asStateFlow()

    fun analyze(barcode: Barcode?) {
        _analyzedFlow.tryEmit(barcode)
    }

    fun scan(barcode: Barcode) {
        when (barcode.valueType) {
            TYPE_TEXT -> {
                val emitted = _scannedFlow.tryEmit(barcode)
                Log.d(TAG, "Emitted state: $emitted")
            }

            else -> Log.e(TAG, "Unsupported barcode format")
        }
    }

    fun clearState() {
        _scannedFlow.value = null
    }

    companion object {
        private val TAG = ScannerViewModel::class.java.simpleName
    }
}
